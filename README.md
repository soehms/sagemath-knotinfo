# SageMath Interface to KnotInfo

Most of the prerelease information which have been shown here is obsolete, since the interface is available (starting with Sage 9.4), now. Therefore, they have been replaced by according informations of the release version.

## Sage Reference Manual Pages

[KnotInfo](https://doc.sagemath.org/html/en/reference/knots/sage/knots/knotinfo.html)
[KnotInfoDataBase](https://doc.sagemath.org/html/en/reference/databases/sage/databases/knotinfo_db.html)

## Python Wrapper

[database_knotinfo](https://github.com/soehms/database_knotinfo#readme)

## Docker Image

If you have Docker installed and about 2.6 GB (download size < 900 MB) of free disk space there is an easy way to use SageMath with the interface to KnotInfo. Just type 

```
docker run -it soehms/my_sagemath:knotinfo_and_snappy
```

This image is built on the stable SageMath 9.1 release and contains beside the new KnotInfo features [SnaPy](https://snappy.math.uic.edu/index.html) installed inside Sage, as well.

## Talks

[LKS-Seminar University of Regensburg, 18.03.2021](introduction_knotinfo.md)
